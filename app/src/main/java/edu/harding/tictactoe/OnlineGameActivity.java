package edu.harding.tictactoe;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class OnlineGameActivity extends Activity {

    private Button returnButton;

    private TextView endGameMessage;

    private static int BOARD_SIZE = 3;
    private ArrayList<Button> cells;

    private LinearLayout noPlayerLayout;

    private LinearLayout boardLayout;
    private TicTacToeConsole game;

    private TextView humanScoreView;
    private TextView androidScoreView;
    private TextView tieScoreView;

    private int win = 0;
    private int buttonDim = 200;
    private int lineDim = 10;

    private List<MediaPlayer> winingSounds;
    private List<MediaPlayer> losingSounds;
    private List<MediaPlayer> tyingSounds;
    private MediaPlayer oSound;
    private MediaPlayer xSound;

    private Vibrator vibrator;

    private SharedPreferences defaultSharedPreferences;

    private Random rand;

    private int boardColor;

    public static final String SERVER_ID_BUNDLE = "SERVER_ID_BUNDLE";
    public static final String PLAYER_BUNDLE = "PLAYER_BUNDLE";
    private String serverId;
    private char player;

    private GameControllerListener gameControllerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        buttonDim = (int) getResources().getDimension(R.dimen.cell_size);

        defaultSharedPreferences = getSharedPreferences("com.example.david.androidtictactoe_preferences", MODE_PRIVATE);

        boardColor = defaultSharedPreferences.getInt("tictactoe_color", Color.BLACK);

        rand = new Random();

        winingSounds = new ArrayList<>();
        losingSounds = new ArrayList<>();
        tyingSounds = new ArrayList<>();

        setSounds();

        cells = new ArrayList<>();

        findViewById(R.id.fullscreen_restart_score_button).setVisibility(View.INVISIBLE);
        findViewById(R.id.fullscreen_restart_button).setVisibility(View.INVISIBLE);

        returnButton = (Button) findViewById(R.id.return_button);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnlineGameActivity.this.onBackPressed();
            }
        });

        noPlayerLayout = (LinearLayout) findViewById(R.id.wait_layout);
        noPlayerLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        endGameMessage = (TextView) findViewById(R.id.end_game_message);
        boardLayout = (LinearLayout) findViewById(R.id.board_layout);

        for (int i = 0; i < BOARD_SIZE; ++i) {
            LinearLayout l = new LinearLayout(this);
            l.setGravity(Gravity.CENTER);
            for (int j = 0; j < BOARD_SIZE; ++j) {
                Button b = new Button(this);
                b.setLayoutParams(new LinearLayout.LayoutParams(buttonDim, buttonDim));
                b.setOnClickListener(new CellClickListener(i, j));
                b.setBackground (null);
                l.addView(b);
                cells.add(b);
                if (j < BOARD_SIZE - 1) {
                    View v = new View(this);
                    v.setLayoutParams (new LinearLayout.LayoutParams (lineDim, buttonDim));
                    v.setBackgroundColor (boardColor);
                    l.addView(v);
                }
            }
            boardLayout.addView(l);
            if (i < BOARD_SIZE - 1) {
                View v = new View(this);
                v.setLayoutParams (new LinearLayout.LayoutParams (buttonDim * BOARD_SIZE + lineDim * 2, lineDim));
                v.setBackgroundColor (boardColor);
                boardLayout.addView(v);
            }
        }

        humanScoreView = (TextView) findViewById(R.id.human_score);
        androidScoreView = (TextView) findViewById(R.id.android_score);
        tieScoreView = (TextView) findViewById(R.id.tie_score);

        humanScoreView.setText("X player: 0");
        androidScoreView.setText("O player: 0");
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.containsKey(SERVER_ID_BUNDLE)) {
            serverId = bundle.getString(SERVER_ID_BUNDLE);
        }
        if (bundle != null && bundle.containsKey(PLAYER_BUNDLE)) {
            player = bundle.getChar(PLAYER_BUNDLE);
        }

        gameControllerListener = new GameControllerListener();

        FirebaseDatabase
                .getInstance()
                .getReference()
                .child("active_matches")
                .child(serverId)
                .addChildEventListener(gameControllerListener);

        game = new TicTacToeConsole(TicTacToeConsole.DifficultyLevel.Online);
    }

    private void updateScores() {
        Map<String, Integer> scores = game.getScores();
        humanScoreView.setText("X player: " + scores.get("HUMAN"));
        androidScoreView.setText("O Player: " + scores.get("ANDROID"));
        tieScoreView.setText("Tie: " + scores.get("TIE"));
    }

    private void clearCells () {
        for (Button b : cells) {
            b.setText("");
            b.setBackground(null);
        }
    }

    private void restartGame () {
        game.restart();
        clearCells();
        endGameMessage.setText("");
        win = 0;
        FirebaseDatabase.getInstance().getReference()
                .child("active_matches")
                .child(serverId)
                .child(player + "_state")
                .setValue("active");
    }

    private class CellClickListener implements View.OnClickListener {

        private int move = -1;

        private CellClickListener(int i, int j) {
            move = i * BOARD_SIZE + j + 1;
        }

        @Override
        public void onClick(View view) {
            if (win == 0 && game.getTurn() == player) {
                if (game.getUserMove(move, player)) {
                    DatabaseReference dr = FirebaseDatabase.getInstance()
                            .getReference()
                            .child("active_matches")
                            .child(serverId);
                    dr.child(player + "_move").setValue(move);

                    if (defaultSharedPreferences.getBoolean ("sound_sound", true)) {
                        xSound.start();
                    }
                    cells.get(move - 1)
                            .setBackgroundResource(player == TicTacToeConsole.X_PLAYER ?
                                    R.drawable.cross_anim : R.drawable.circle_anim);
                    ((AnimationDrawable)cells.get(move - 1).getBackground()).start();
                    game.nextTurn();
                    checkForWinner(true);
                }
            }
        }
    }

    private class GameControllerListener implements ChildEventListener {

        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            String key = dataSnapshot.getKey();
            if (key.equals((char)(player ^ TicTacToeConsole.O_PLAYER ^ TicTacToeConsole.X_PLAYER) + "_state")) {
                String val = dataSnapshot.getValue(String.class);
                if (val.equals("no_active")) {
                    noPlayerLayout.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            String key = dataSnapshot.getKey();
            int move;
            switch (key) {
                case "X_move":
                    move = dataSnapshot.getValue(Integer.class);
                    if (game.getUserMove(move, TicTacToeConsole.X_PLAYER)) {
                        if (defaultSharedPreferences.getBoolean ("sound_sound", true)) {
                            xSound.start();
                        }
                        cells.get(move - 1).setBackgroundResource(R.drawable.cross_anim);
                        ((AnimationDrawable)cells.get(move - 1).getBackground()).start();
                        game.nextTurn();
                        checkForWinner(false);
                    }
                    break;
                case "O_move":
                    move = dataSnapshot.getValue(Integer.class);
                    if (game.getUserMove(move, TicTacToeConsole.O_PLAYER)) {
                        if (defaultSharedPreferences.getBoolean ("sound_sound", true)) {
                            xSound.start();
                        }
                        cells.get(move - 1).setBackgroundResource(R.drawable.circle_anim);
                        ((AnimationDrawable)cells.get(move - 1).getBackground()).start();
                        game.nextTurn();
                        checkForWinner(false);
                    }
                    break;
            }
            if (key.equals((char)(player ^ TicTacToeConsole.O_PLAYER ^ TicTacToeConsole.X_PLAYER) + "_state")) {
                String val = dataSnapshot.getValue(String.class);
                if (val.equals("no_active")) {
                    //noPlayerLayout.setVisibility(View.VISIBLE);
                }
                else {
                    noPlayerLayout.setVisibility(View.INVISIBLE);
                }
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        public void onCancelled(DatabaseError databaseError) {
            onBackPressed();
        }
    }

    private void checkForWinner(final boolean checkForPlayers) {
        win = game.checkForWinner();

        if (win != 0) {
            game.updateScore();

            if (win == 1) {
                endGameMessage.setText(R.string.tie);
                if (defaultSharedPreferences.getBoolean("sound_sound", true)) {
                    tyingSounds.get(rand.nextInt(tyingSounds.size())).start();
                }
            } else if (win == 2) {
                endGameMessage.setText(R.string.x_player_won);
                if (defaultSharedPreferences.getBoolean("sound_sound", true)) {
                    winingSounds.get(rand.nextInt(winingSounds.size())).start();
                }

            } else if (win == 3) {
                endGameMessage.setText(R.string.o_player_won);
                if (defaultSharedPreferences.getBoolean("sound_sound", true)) {
                    losingSounds.get(rand.nextInt(losingSounds.size())).start();
                }
            } else {
                endGameMessage.setText(R.string.error);
            }

            if (defaultSharedPreferences.getBoolean("sound_vibrate", true)) {
                startVibrate();
            }
            updateScores();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (checkForPlayers) {
                        FirebaseDatabase.getInstance().getReference()
                                .child("active_matches")
                                .child(serverId)
                                .child((char)(player ^ TicTacToeConsole.X_PLAYER ^ TicTacToeConsole.O_PLAYER) + "_state")
                                .setValue("no_active");
                    }
                    restartGame();
                }
            }, 2000);
        }
    }

    private void setSounds () {
        winingSounds.add(MediaPlayer.create(this, R.raw.win1));
        losingSounds.add(MediaPlayer.create(this, R.raw.lose1));
        tyingSounds.add(MediaPlayer.create(this, R.raw.tie1));
        xSound = MediaPlayer.create(this, R.raw.o_sound);
        oSound = MediaPlayer.create(this, R.raw.o_sound);
    }

    public void startVibrate() {
        vibrator = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);
        vibrator.vibrate(500);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSounds();
        FirebaseDatabase.getInstance().getReference().child("active_matches")
                .child(serverId)
                .addChildEventListener(gameControllerListener);
    }

    @Override
    protected void onPause() {
        super.onPause();

        for (MediaPlayer player : winingSounds) {
            player.release();
        }
        for (MediaPlayer player : losingSounds) {
            player.release();
        }
        for (MediaPlayer player : tyingSounds) {
            player.release();
        }

        winingSounds.clear();
        losingSounds.clear();
        tyingSounds.clear();
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseDatabase.getInstance().getReference().child("active_matches")
                .child(serverId)
                .removeEventListener(gameControllerListener);
        FirebaseDatabase.getInstance().getReference().child("open_matches").child(serverId).removeValue();
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child("active_matches")
                .child(serverId)
                .child(player + "_state")
                .setValue("no_active");
    }
}