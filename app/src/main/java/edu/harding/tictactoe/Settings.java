/*
 * Copyright (C) 2010 By Frank McCown at Harding University
 * 
 * This is the solution to Tutorial 6.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.harding.tictactoe;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;

public class Settings extends PreferenceActivity implements
		ColorPickerDialog.OnColorChangedListener {
	
	public static final String SOUND_PREFERENCE_KEY = "sound";
	public static final String GOES_FIRST_PREFERENCE_KEY = "goes_first";
	public static final String BOARD_COLOR_PREFERENCE_KEY = "board_color";
	public static final String DIFFICULTY_PREFERENCE_KEY = "difficulty_level";
	public static final String VICTORY_MESSAGE_PREFERENCE_KEY = "victory_message";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
							
		final ListPreference goesFirstPref = (ListPreference) findPreference(GOES_FIRST_PREFERENCE_KEY);
		String goesFirst = prefs.getString(GOES_FIRST_PREFERENCE_KEY, "Alternate");
		goesFirstPref.setSummary((CharSequence) goesFirst);
		goesFirstPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				goesFirstPref.setSummary((CharSequence) newValue);

				// Since we are handling the pref, we must save it
				SharedPreferences.Editor ed = prefs.edit();
				ed.putString(GOES_FIRST_PREFERENCE_KEY, newValue.toString());
				ed.commit();
				
				return true;
			}
		});		
		
		final ListPreference difficultyLevelPref = (ListPreference) 
				findPreference(DIFFICULTY_PREFERENCE_KEY);
		String difficulty = prefs.getString(DIFFICULTY_PREFERENCE_KEY, 
				getResources().getString(R.string.difficulty_expert));
		difficultyLevelPref.setSummary((CharSequence) difficulty);
		difficultyLevelPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				difficultyLevelPref.setSummary((CharSequence) newValue);

				// Since we are handling the pref, we must save it
				SharedPreferences.Editor ed = prefs.edit();
				ed.putString(DIFFICULTY_PREFERENCE_KEY, newValue.toString());
				ed.commit();
				
				return true;
			}
		});
		
		final EditTextPreference victoryMessagePref = (EditTextPreference) 
				findPreference(VICTORY_MESSAGE_PREFERENCE_KEY);
		String victoryMessage = prefs.getString(VICTORY_MESSAGE_PREFERENCE_KEY, getResources().getString(R.string.result_human_wins));
		victoryMessagePref.setSummary("\"" + victoryMessage + "\"");
		victoryMessagePref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				victoryMessagePref.setSummary((CharSequence) newValue);

				SharedPreferences.Editor ed = prefs.edit();
				ed.putString(VICTORY_MESSAGE_PREFERENCE_KEY, newValue.toString());
				ed.commit();
				
				return true;
			}
		});
		
		
		final Preference boardColorPref = (Preference) findPreference(BOARD_COLOR_PREFERENCE_KEY);
		boardColorPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
            public boolean onPreferenceClick(Preference preference) { 
				int color = prefs.getInt(BOARD_COLOR_PREFERENCE_KEY, Color.GRAY);
				new ColorPickerDialog(Settings.this, Settings.this,
                        color).show();
                    return true;
            }
		});
		
	}
	
	@Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialog = null; // = new ColorPickerDialog();
		return dialog;
	}

	@Override
	public void colorChanged(int color) {   
		PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(
			   BOARD_COLOR_PREFERENCE_KEY, color).commit();		
	}
}
